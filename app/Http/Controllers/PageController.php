<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class PageController extends Controller
{
    //hello() -> is a method.
    public function hello(){
        //2 WAYS OF PASSING VALUES TO A VIEW:
            //when you return 'hello.blade.php' you will run it ->with key value pair: 'name', 'Homer Simpson'
                //return view('hello')->with('name', 'Homer Simpson'); //this calls the views folder => 'hello.blade.php'
                //return view('hello', ['name' => 'Homer SIMPSON']);  //2ND WAY

        //PASSING MULTIPLE VALUES TO A VIEW:
            $info = array(
                'frontend' => 'Zuitt Coding Bootcamp', 
                'topics' => ['HTML and CSS', 'JS DOM', 'React']
            );
            return view('hello')->with($info);
    }

    public function about(){
        return view('pages/about');
    }

    public function services(){
        return view('pages/services');
    }

    public function index(){
        return view('pages/index');
    }
}
